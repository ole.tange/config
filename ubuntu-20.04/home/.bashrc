# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

PATH=$HOME/bin:$PATH:/sbin:/usr/sbin:$HOME/.local/bin:$HOME/.cargo/bin
export PATH

# If not running interactively, skip the rest
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# dir without cd does cd
shopt -s autocd

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

set_color_prompt() {
    _colorcombos() {
	PERL_HASH_SEED=9 perl -MB -e '
		use B;
		# color combinations that are readable (e.g. no red on red)
		@c =((map { "$_\n0\n" }
		     6..7,9..11,13..15,40..51,75..87,113..123,147..159,171..231,249..254),
		    (map { "$_\n231\n" }
		     1..9,12..13,16..45,52..81,88..116,124..151,153,160..180,
		     182..185,187..189,196..214,232..252,255..254));
		for(@ARGV) {
                    srand(hex(B::hash($_))+$ENV{PERL_HASH_SEED});
                    print @c[rand()* ($#c+1)];
		}
		' "$@"
	}
    local col=($(_colorcombos `whoami` `hostname` "`id`"))
    # (bg1,fg1)=user, (bg2,fg2)=host, (bg3,fg3)=path
    PS1='${debian_chroot:+($debian_chroot)}\[\033[48;5;'${col[0]}';38;5;'${col[1]}'m\]\u\[\033[00m\]\[\033[48;5;'${col[2]}';38;5;'${col[3]}'m\]@\h\[\033[00m\]:\[\033[48;5;'${col[4]}';38;5;'${col[5]}'m\]\w\[\033[00m\]\$ '
}

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color)
    set_color_prompt
    ;;
xterm-256color)
    set_color_prompt
    ;;
screen-256color)
    set_color_prompt
    ;;
screen)
    set_color_prompt
    ;;
*)
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    ;;
esac

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
    ;;
*)
    ;;
esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    #alias dir='ls --color=auto --format=vertical'
    #alias vdir='ls --color=auto --format=long'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#fi

LS_COLORS='no=00:fi=00:di=01;34:ln=01;35:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;31:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.flac=01;35:*.mp3=01;35:*.mpc=01;35:*.ogg=01;35:*.wav=01;35:';
export LS_COLORS

EDITOR=em
export EDITOR
VISUAL=em
export VISUAL
LESS=-Mqnfr
export LESS

DEBEMAIL=ole@tange.dk
DEBFULLNAME="Ole Tange"
export DEBEMAIL DEBFULLNAME

alias gi="grep -i"

# Add sshkey to sshagent unless already done
ssha() {
    if [ -e ~/.ssh/SSH_AUTH_SOCK ] ; then
	export SSH_AUTH_SOCK=`cat ~/.ssh/SSH_AUTH_SOCK`
    fi
    if [ -e ~/.ssh/SSH_AGENT_PID ] ; then
	export SSH_AGENT_PID=`cat ~/.ssh/SSH_AGENT_PID`
    fi
    if ssh-add -l ; then
	true
    else
	eval `ssh-agent` ssh-add ~/.ssh/id*[^b] &&
	    echo $SSH_AUTH_SOCK > ~/.ssh/SSH_AUTH_SOCK &&
	    echo $SSH_AGENT_PID > ~/.ssh/SSH_AGENT_PID
    fi
}
ssha
#export MOZ_NO_REMOTE=1

LC_ALL=en_US.UTF-8
bind 'set convert-meta off'

PERL_MB_OPT="--install_base \"/home/tange/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/tange/perl5"; export PERL_MM_OPT;
sh -c '(killall ibus-x11 2>/dev/null &)'

tallia() {
    echo "select '2016-06-21'::timestamp - now();" |sql pg:///
}
. `which env_parallel.bash`
shopt -s globstar
GOPATH=$HOME/go
export GOPATH
MANPATH=$MANPATH:$HOME/share/man

mgrep() {
    grep -rzlP "(?s)$(printf "(?=.*?%s)" "$@")" .
}

m2grep() {
    v="$1"
    shift
    _mgrep() {
	xargs -d "\n" grep -l "$1";
    }
    find . -type f | parallel -kXq grep -l "$v" |
	eval "cat $(printf "|_mgrep \"%s\"" "$@")"
}

(play -q -n synth sine F2 sine C3 remix - fade 0 4 .1 norm -20 bend 0.5,2477,2 fade 0 4.0 0.5 2>/dev/null &) 2>/dev/null

alias y='idok -port $((RANDOM+2000))'

# gqview ui?
#export QT_SCALE_FACTOR=2
MANPATH=$MANPATH:$HOME/share/man
if [ -f ~/.passwords ]; then
    . ~/.passwords
fi

cat <<EOF | bash 2>/dev/null >/dev/null
(
    start_tor_and_ssh() {
	start_sshd_and_tor() {
	    sh -c `which sshd`' -p 2222 &'
	    sh -c 'tor &'
	}
	sleep 10
	# Start tor sshd on port 2222
	# But only if $HOME/.started-bash is older than 8.6 seconds
	if [ -e $HOME/.started-bash ] ; then
	    if perl -e 'exit (0+ -M "'$HOME'/.started-bash" > 0.0001) '; then
		# Short time 8.6 sec since last bash: do not start sshd
		touch $HOME/.started-bash
	    else
		start_sshd_and_tor
	    fi
	else
	    start_sshd_and_tor
	fi
	touch $HOME/.started-bash
    }
    
    change_keymap() {
	if [ "$DISPLAY" != "" ] ; then
	    # Set Menu key = Mouse Button 3
	    # Lenovo back/forward => PgUp/PgDown
	    xmodmap -e 'keycode 166=Prior' 2>/dev/null
	    xmodmap -e 'keycode 167=Next' 2>/dev/null
	    xmodmap -e "pointer = 1 3 2" 2>/dev/null
	    
	    xkbset m
	    xkbset exp =m
	    xkbcomp "$DISPLAY" - |
		# Menu key = mouse button
		# key <COMP> {         [ Menu ] };
		# =>
		# key <COMP> {         [ Pointer_Button2 ] };
		perl -pe '/key <COMP>/ && s/\[.*\]/ [ Pointer_Button2 ]/' |
		perl -pe '/key <RWIN>/ && s/\[.*\]/ [ Pointer_Button1 ]/' |
		sed -e '/interpret KP_/,/};/d' |
		# Replace numpad , with .
		sed -e 's/ KP_Separator / period /' |
		xkbcomp - "$DISPLAY" 2> >(grep -v 'No symbols defined for')
	fi
    }
    sleep 3
    change_keymap
    start_tor_and_ssh
) &
EOF

IO() {
    # Minimize output from iostat -dkx 1
    # Usage:
    #   IO [substring]
    string="${1:-sd}"
    iostat -dkx 1 |
	perl -ne 'BEGIN { $| = 1; $string = shift }
   	    s/(........)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)/$1$3$9$21/
	    ||
	    s/(........)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)/$1$4$5$16/
            ||
            s/(........)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)(\s+\S+)/$1$6$7$14/;
	    /Device/ and print and next;
	    m^$string^ and print;
	' $string;
}
ORACLE_HOME=/usr/lib/oracle/19.6/client64
PATH=$PATH:$ORACLE_HOME/bin
export ORACLE_HOME
export ORACLE_SID=XE
ppid() {
    for _pid in "$@" ; do
	ps -o ppid -p "$_pid" | tail -n1;
    done
}

# Add dotnet + go binaries
export PATH="$PATH:$HOME/.dotnet/tools:$HOME/go/bin"

PLAN9=/usr/local/plan9 export PLAN9
PATH=$PATH:$PLAN9/bin export PATH
export DRAC_HOST=drac.lan
LBRY_YOUTUBEDL="torsocks yt-dlp"
export LBRY_YOUTUBEDL
YOUTUBE="yt-dlp"
export YOUTUBE
export SMALLDISK=/mnt/ram
