#!/bin/bash

# Boot Live CD
# Start terminal
# sudo su -
# apt update
# apt install openssh-server
# passwd ubuntu
#
# read secretpassword
# password=`cat /mount/cryptkey.txt`

secretpassword=${secretpassword:-MyLUKSPassword}
password=${password:-MyLUKSPassword}
rootpassword=${rootpassword:-MyRootPassword}
hostname=${hostname:-myhostname}
DISK=${DISK:-/dev/disk/by-id/ata-ST1000LM024_HN-M101MBB_S2R8JX0D400082}

echo "$hostname $password $rootpassword $secretpassword $DISK"

export rootpassword
export password
export DISK

install_build_software() {
    apt-add-repository universe
    apt update
    apt install --yes debootstrap gdisk zfs-initramfs cryptsetup-bin
}

partitiondisk() {
    sgdisk --zap-all $DISK
    sgdisk            -n2:1M:+510M     -t2:EF00 $DISK
    sgdisk -a 1048576 -n3:0:+2G        -t3:BF01 $DISK
    end_position=$(sgdisk -E $DISK)
    sgdisk -a 1048576 -n4:0:$(( $end_position - (($end_position + 1) % 2048) )) -t4:BF01 $DISK
    fdisk -l $DISK
    # Needed for partitiontable to be visible
    sleep 5
    partprobe
}

setup_zpool_for_boot() {
    zpool destroy bpool 2>/dev/null
    zpool create -f -o ashift=12 -d \
	  -o feature@async_destroy=enabled \
	  -o feature@bookmarks=enabled \
	  -o feature@embedded_data=enabled \
	  -o feature@empty_bpobj=enabled \
	  -o feature@enabled_txg=enabled \
	  -o feature@extensible_dataset=enabled \
	  -o feature@filesystem_limits=enabled \
	  -o feature@hole_birth=enabled \
	  -o feature@large_blocks=enabled \
	  -o feature@lz4_compress=enabled \
	  -o feature@spacemap_histogram=enabled \
	  -o feature@userobj_accounting=enabled \
	  -O acltype=posixacl -O canmount=off -O compression=lz4 -O devices=off \
	  -O normalization=formD -O relatime=on -O xattr=sa \
	  -O mountpoint=/ -R /mnt bpool ${DISK}-part3
}

setup_zpool_for_root() {
    zpool destroy rpool 2>/dev/null
    cryptsetup luksClose luks1
    echo "$password" | cryptsetup -y -v luksFormat --sector-size 4096 \
				  --pbkdf-parallel 1 \
				  --pbkdf-memory 4000000 --pbkdf argon2id --iter-time 1000 \
				  ${DISK}-part4
    cryptsetup config --priority prefer --key-slot 0 ${DISK}-part4
    echo "$password" | cryptsetup luksOpen ${DISK}-part4 luks1
    (echo "$password"; echo "$secretpassword") |
	cryptsetup -y -v luksAddKey \
				  --pbkdf-parallel 1 \
				  --pbkdf-memory 4000000 --pbkdf argon2id --iter-time 40000 \
				  ${DISK}-part4
    zpool create -o ashift=12 \
	  -O acltype=posixacl -O canmount=off -O compression=lz4 \
	  -O dnodesize=auto -O normalization=formD -O relatime=on -O xattr=sa \
	  -O mountpoint=/ -R /mnt rpool /dev/mapper/luks1
}

create_zfs_mounts() {
    zfs create -o canmount=off -o mountpoint=none rpool/ROOT
    zfs create -o canmount=off -o mountpoint=none bpool/BOOT

    zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/ubuntu
    zfs mount rpool/ROOT/ubuntu

    zfs create -o canmount=noauto -o mountpoint=/boot bpool/BOOT/ubuntu
    zfs mount bpool/BOOT/ubuntu
}

bootstrap_debian() {
    debootstrap focal /mnt
    # Do not allow device files in rpool (why?)
    zfs set devices=off rpool
}


make_stage2() {
    cat <<_stage2_eof >/mnt/stage2.sh
set_hostname() {
    echo $hostname > etc/hostname
    echo 127.0.1.1 $hostname >> etc/hosts
}

add_apt_sources() {
    perl -pe 's/\s*$/\n/' <<EOF > etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu focal main universe
deb-src http://archive.ubuntu.com/ubuntu focal main universe

deb http://security.ubuntu.com/ubuntu focal-security main universe
deb-src http://security.ubuntu.com/ubuntu focal-security main universe

deb http://archive.ubuntu.com/ubuntu focal-updates main universe
deb-src http://archive.ubuntu.com/ubuntu focal-updates main universe
EOF

    ln -s /proc/self/mounts /etc/mtab
    apt update

    locale-gen --purge "en_US.UTF-8"
    update-locale LANG=en_US.UTF-8 LANGUAGE=en_US
    dpkg-reconfigure --frontend noninteractive locales

#dpkg-reconfigure tzdata

}


install_initrd_tools() {
    apt install --yes nano
    apt install linux-modules-5.4.0-26-generic
    apt install --yes --no-install-recommends linux-image-generic
    apt install --yes zfs-initramfs
    apt install --yes grub-efi-amd64
#    apt install --yes grub-pc
}

install_luks() {
    apt install --yes cryptsetup
    # Add LUKS device for root in /etc/crypttab
    echo luks1 UUID=$(blkid -s UUID -o value ${DISK}-part4) none \
	 luks,discard,initramfs > /etc/crypttab
}

install_efi() {
    umount /boot/efi
    apt install dosfstools
    mkdosfs -F 32 -s 1 -n EFI ${DISK}-part2
    mkdir -p /boot/efi
    echo PARTUUID=$(blkid -s PARTUUID -o value ${DISK}-part2) \
	 /boot/efi vfat nofail,x-systemd.device-timeout=1 0 1 >> /etc/fstab
    mount /boot/efi
    apt install --yes grub-efi-amd64-signed shim-signed
}

install_zfs_systemd_service() {
    perl -pe 's/\s*$/\n/' <<EOF > /etc/systemd/system/zfs-import-bpool.service
[Unit]
DefaultDependencies=no
Before=zfs-import-scan.service
Before=zfs-import-cache.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/zpool import -N -o cachefile=none bpool

[Install]
WantedBy=zfs-import.target

EOF

    systemctl enable zfs-import-bpool.service
}

adduser_group() {
    addgroup --system lpadmin
    addgroup --system sambashare
    echo "root:$rootpassword" | chpasswd
}

install_grub() {
    grub-probe /boot

    echo "### These are OK:"
    echo "   cryptsetup: ERROR: Couldn't resolve device rpool/ROOT/ubuntu"
    echo "   cryptsetup: WARNING: Couldn't determine root device"
    update-initramfs -c -k all

    (
	echo GRUB_TERMINAL=console
	echo GRUB_TIMEOUT=5
	echo 'GRUB_CMDLINE_LINUX="root=ZFS=rpool/ROOT/ubuntu"'
	echo GRUB_TIMEOUT_STYLE=''
	echo 'GRUB_CMDLINE_LINUX_DEFAULT=""'
	echo GRUB_TIMEOUT=5
    ) >>/etc/default/grub

    update-grub

    grub-install --target=x86_64-efi --efi-directory=/boot/efi \
		 --bootloader-id=ubuntu --recheck --no-floppy ||
    grub-install --target=i386-pc \
		 --bootloader-id=ubuntu --recheck --no-floppy
}

ready_for_first_boot() {
    zpool export bpool
    zpool export rpool
    echo "Now reboot"
    echo "You may have to do this on first boot"
    echo "   zpool import -f bpool"
    echo "   zpool import -f rpool"
}

stage2() {
    set_hostname
    add_apt_sources
    install_initrd_tools
    install_luks
    install_efi
    install_zfs_systemd_service
    adduser_group
    install_grub
    ready_for_first_boot
}

stage2
_stage2_eof
}

stage1() {
    install_build_software
    partitiondisk
    setup_zpool_for_boot
    setup_zpool_for_root
    create_zfs_mounts
    bootstrap_debian
    make_stage2
}

umount_all() {
    lazy_umount() {
	umount "$@" || umount -l "$@"
    }
    lazy_umount /mnt/boot/efi
    lazy_umount umount /mnt/boot
    lazy_umount /mnt/sys/fs/cgroup/unified
    lazy_umount /mnt/sys/fs/cgroup
    lazy_umount /mnt/sys
    lazy_umount /mnt/dev/pts
    umount /mnt/proc /mnt/sys /mnt/dev
    zpool export bpool || zpool export -f bpool
    umount /mnt || umount -l /mnt
    zpool export rpool || zpool export -f rpool
}


doall() {
    stage1    
    modprobe efivars
    mount --rbind /dev  /mnt/dev
    mount --rbind /proc /mnt/proc
    mount --rbind /sys  /mnt/sys
    chroot /mnt /usr/bin/env DISK=$DISK bash -x /stage2.sh
    umount_all
}
